﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum jenistutorial
{
    tutorialcharacter,
    tutorialhomepage,
    tutorialhiragana,
    tutorialnewgame,
}

public class Dialogue : MonoBehaviour {

    public jenistutorial Jenistutorial;

    public Text tutorialdisplay;
    public int id = 0;
    public GameObject arrowtext;
    [TextArea(2, 4)]
    public string[] tutordialogue;
    public GameObject[] arrowguide;
    private string alreadyplay;
    private bool dialogRun;

    // Use this for initialization

    private void Start()
    {
       if(Jenistutorial == jenistutorial.tutorialcharacter)
        {
            alreadyplay = PlayerPrefs.GetString("characterdone");
        }
        else if(Jenistutorial == jenistutorial.tutorialhomepage)
        {
            alreadyplay = PlayerPrefs.GetString("homepagedone");
        }
        else if (Jenistutorial == jenistutorial.tutorialhiragana)
        {
            alreadyplay = PlayerPrefs.GetString("hiraganadone");
        }


        if (alreadyplay == "Yes")
        {
            gameObject.SetActive(false);
        }
        else
        {
            if (Jenistutorial == jenistutorial.tutorialcharacter)
            {
                id = 0;
                tutorialcharacter();
            }
            else if (Jenistutorial == jenistutorial.tutorialhomepage)
            {
                id = 0;
                tutorialhomepage();
            }
            else if (Jenistutorial == jenistutorial.tutorialhiragana)
            {
                id = 0;
                tutorialtabelhiragana();
            }
        }
    }

    private void OnEnable()
    {
        if (Jenistutorial == jenistutorial.tutorialcharacter)
        {
            id = 0;
            tutorialcharacter();
        }
        else if (Jenistutorial == jenistutorial.tutorialhomepage)
        {
            id = 0;
            tutorialhomepage();
        }
        else if (Jenistutorial == jenistutorial.tutorialhiragana)
        {
            id = 0;
            tutorialtabelhiragana();
        }
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void tutorialcharacter()
    {
        
        
        if(id < tutordialogue.Length)
        {
            matiinpanah();
            arrowguide[id].SetActive(true);


                tutorialdisplay.text = "";
                tutorialdisplay.text = tutordialogue[id];
                id++;
                arrowtext.SetActive(true);
                
        }   
        else
        {
            StartCoroutine(showtext(id, "characterdone"));
        }
    }

    public void tutorialhomepage()
    {
        if (id < tutordialogue.Length)
        {
            matiinpanah();
            arrowguide[id].SetActive(true);

            tutorialdisplay.text = "";
                tutorialdisplay.text = tutordialogue[id];
                
                id++;
                
                arrowtext.SetActive(true);
                
            
        }
        else
        {
            StartCoroutine(showtext(id, "homepagedone"));
        }
    }

    public void tutorialtabelhiragana()
    {

        if (id < tutordialogue.Length)
        {
            matiinpanah();
            arrowguide[id].SetActive(true);
            tutorialdisplay.text = "";
                tutorialdisplay.text = tutordialogue[id];
             
                id++;
                arrowtext.SetActive(true);
                
        }
        else
        {
            StartCoroutine(showtext(id, "hiraganadone"));
        }
    }



    IEnumerator showtext(int id, string namasave)
    {
        dialogRun = true;
       if(id < tutordialogue.Length)
       {
            matiinpanah();
            tutorialdisplay.text = "";
            arrowguide[id].SetActive(true);
            foreach (char a in tutordialogue[id])
            {
                tutorialdisplay.text += a;
                yield return new WaitForSeconds(0.02f);
            }
        }
        else
        {
            PlayerPrefs.SetString(namasave, "Yes");
            gameObject.SetActive(false);
        }
        dialogRun = false;    
        
    }

    void matiinpanah()
    {
        for (int i = 0;i < arrowguide.Length;i++)
        {
            arrowguide[i].SetActive(false);
        }
    }
}
