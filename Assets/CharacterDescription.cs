﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterDescription : MonoBehaviour {
    public Text deskripsidisplay;
    public Button avatar;
    public GameObject inputNama;
    public GameObject ButtonSave;
    public GameObject ButtonCreate;
    public Sprite[] avatarimage;
    [TextArea(2,6)]public string[] deskripsikarakter;//areatext untuk deskripsi main menu di inspector unity

    private int characterid;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowDescription (int id)
    {
        characterid = id;
        SaveLoadData.instance.Playerdatas.objectid = id;
        StartCoroutine(showtext(id));
        ChangeActBaseOnCharacter();
    }

    IEnumerator showtext(int id)
    {
        deskripsidisplay.text = "";
        foreach (char a in deskripsikarakter[id])
        {
            deskripsidisplay.text += a;
            yield return new WaitForSeconds(0.02f);
        }

    }
    public void selectcharacter ()
    {
        avatar.image.sprite = avatarimage[characterid];
    }

    public void OpenChangecharacter()
    {
        inputNama.SetActive(false);
        ButtonCreate.SetActive(false);
        ButtonSave.SetActive(true);
    }

    void ChangeActBaseOnCharacter()
    {
        if(characterid == 0)
        {
            SaveLoadData.instance.Playerdatas.actID = 0;
        }
        else if(characterid == 1)
        {
            SaveLoadData.instance.Playerdatas.actID = 4;
        }
        else if(characterid == 2)
        {
            SaveLoadData.instance.Playerdatas.actID = 8;
        }
    }
}

