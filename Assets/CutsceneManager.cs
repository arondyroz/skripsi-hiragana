﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CutsceneManager : MonoBehaviour {
    public Image backgroundAct;
    public Image character;
    public Text displaydialogue;
    public Sprite[] gambarAct;
    public Sprite[] CharacterImage; //0 = system, 1 = Ryu, 2 = Mother, 3 = Village Head, 4= Doctor, 5 = Akahaito,s Army1 = 6, Akahito's army 2 = 7, Army's Leader = 8
    //public Sprite[] EndingImage; //0 = char1, 1 = char2, 2 = char 3
    [TextArea(3,6)] public string[] dialogueAct1; //Character 1, dialogue 1
    [TextArea(3,6)] public string[] dialogueAct2; //Character 1, dialogue 2
    [TextArea(3,6)] public string[] dialogueAct3; //Character 1, dialogue 3
    [TextArea(3,6)] public string[] dialogueAct4; //Character 1, ending
    [TextArea(3,6)] public string[] dialogueAct5; //Character 2, dialogue 1
    [TextArea(3,6)] public string[] dialogueAct6; //Character 2, dialogue 2
    [TextArea(3,6)] public string[] dialogueAct7; //Character 2, dialogue 3
    [TextArea(3,6)] public string[] dialogueAct8; //Character 2, ending
    [TextArea(3,6)] public string[] dialogueAct9; //Character 3, dialogue 1
    [TextArea(3,6)] public string[] dialogueAct10; //Character 3, dialogue 2
    [TextArea(3,6)] public string[] dialogueAct11; //Character 3, dialogue 3
    [TextArea(3,6)] public string[] dialogueAct12; //Character 3, ending
    private int textID = 0;
    private string namascene = "Question 1 Scene";
    private string namascene2 = "Question 4 TA Scene";
    private string namascene3 = "Question 7 MA Scene";
    // Use this for initialization
    void Start () {
        loadAct();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void loadAct()
    {
        //if (SaveLoadData.instance.Playerdatas.actID < 3 )|| SaveLoadData.instance.Playerdatas.actID > 7 && SaveLoadData.instance.Playerdatas.actID < 10) 
        //{
            backgroundAct.sprite = gambarAct[SaveLoadData.instance.Playerdatas.actID];
        //}
        //else if(SaveLoadData.instance.Playerdatas.actID > 3 && SaveLoadData.instance.Playerdatas.actID < 7)
        //{

        //}
        //else if (SaveLoadData.instance.Playerdatas.actID == 3 || SaveLoadData.instance.Playerdatas.actID == 7 || SaveLoadData.instance.Playerdatas.actID == 10)
        //{
        //    backgroundAct.sprite = EndingImage[0];
        //}
        loadDialogue();
    }
    public void loadDialogue()
    {
        string Arrayhuruf = "a";
        int id = SaveLoadData.instance.Playerdatas.actID;
        if (id == 0)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct1.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct1[textID];
                Arrayhuruf = dialogueAct1[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene);
            }
        }
        else if (id == 1)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct2.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct2[textID];
                Arrayhuruf = dialogueAct2[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene2);
            }
        }
        else if (id == 2)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct3.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct3[textID];
                Arrayhuruf = dialogueAct3[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene3);
            }
        }
        else if (id == 3)
        {
            Debug.Log("nomor id: " + id);
            SaveLoadData.instance.Playerdatas.unlockedcharacter = 2;
            SaveLoadData.instance.SaveData();
            if (textID < dialogueAct4.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct4[textID];
                Arrayhuruf = dialogueAct4[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene("Menu Scene");
            }
        }
        else if (id == 4)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct5.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct5[textID];
                Arrayhuruf = dialogueAct5[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene);
            }
        }
        else if (id == 5)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct6.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct6[textID];
                Arrayhuruf = dialogueAct6[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene2);
            }
        }
        else if (id == 6)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct7.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct7[textID];
                Arrayhuruf = dialogueAct7[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene3);
            }
        }
        else if (id == 7)
        {
            Debug.Log("nomor id: " + id);
            SaveLoadData.instance.Playerdatas.unlockedcharacter = 3;
            SaveLoadData.instance.SaveData();
            if (textID < dialogueAct8.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct8[textID];
                Arrayhuruf = dialogueAct8[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene("Menu Scene");
            }
        }
        else if (id == 8)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct9.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct9[textID];
                Arrayhuruf = dialogueAct9[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene);
            }
        }
        else if (id == 9)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct10.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct10[textID];
                Arrayhuruf = dialogueAct10[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene2);
            }
        }
        else if (id == 10)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct11.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct11[textID];
                Arrayhuruf = dialogueAct11[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene(namascene3);
            }
        }
        else if (id == 11)
        {
            Debug.Log("nomor id: " + id);
            if (textID < dialogueAct12.Length)
            {
                //matiinpanah();
                //arrowguide[id].SetActive(true);


                displaydialogue.text = "";
                displaydialogue.text = dialogueAct12[textID];
                Arrayhuruf = dialogueAct12[textID];
                gantiAvatarDialogue(Arrayhuruf);
                textID++;
                //arrowtext.SetActive(true);

            }
            else
            {
                SceneManager.LoadScene("Menu Scene");
            }
        }
    }
    void gantiAvatarDialogue(string Arrayhuruf)
    {
        if (Arrayhuruf.Length > 0)
        {
            if (Arrayhuruf[1].ToString() == "s" || Arrayhuruf[1].ToString() == "S")
            {
                character.sprite = CharacterImage[0];
            }
            else if (Arrayhuruf[1].ToString() == "r" || Arrayhuruf[1].ToString() == "R")
            {
                character.sprite = CharacterImage[1];
            }
            else if (Arrayhuruf[1].ToString() == "m" || Arrayhuruf[1].ToString() == "M")
            {
                character.sprite = CharacterImage[2];
            }
            else if (Arrayhuruf[1].ToString() == "v" || Arrayhuruf[1].ToString() == "V")
            {
                character.sprite = CharacterImage[3];
            }
            else if (Arrayhuruf[1].ToString() == "d" || Arrayhuruf[1].ToString() == "D")
            {
                character.sprite = CharacterImage[4];
            }
        }
    }
}
