﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pertanyaan
{
    
    public string jawaban1;
    public string jawaban2;
    public string jawaban3;
    public string jawaban4;
    public int idjawaban;
    public Sprite pertanyaan;
}
