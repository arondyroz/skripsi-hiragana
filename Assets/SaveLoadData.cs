﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveLoadData : MonoBehaviour {

    public Playerdata Playerdatas;
    [HideInInspector] public string SaveName;
    public int objectid;
    public static SaveLoadData instance;

    private void Awake() //awake ngejalanin perintah sebelom scene dimulai
    {
        instance = this;

        if(Application.platform == RuntimePlatform.Android)
        {
            SaveName = "\\datagame.Json";
        }
        else
        {
            SaveName = "/datagame.Json";
        }

        LoadData();

    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	if(Input.GetKeyDown(KeyCode.F3))
        {
            if(File.Exists(Application.persistentDataPath + SaveName))
            {
                File.Delete(Application.persistentDataPath + SaveName);
            }
            PlayerPrefs.DeleteAll();
        }
        
	}

    public void SaveData()
    {
        Playerdata savedata = Playerdatas;
        var filenya = JsonUtility.ToJson(savedata);
        File.WriteAllText(Application.persistentDataPath + SaveName, filenya);

    }
    public void LoadData()
    {
        if(File.Exists(Application.persistentDataPath + SaveName))
        {
            var loadgame = File.ReadAllText(Application.persistentDataPath + SaveName);
            Playerdata datapemain = JsonUtility.FromJson<Playerdata>(loadgame);

            Playerdatas = datapemain;
        }
    }
}
