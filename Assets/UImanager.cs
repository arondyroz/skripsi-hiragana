﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class UImanager : MonoBehaviour {

    // Use this for initialization
    public GameObject avatarpanel;
    public GameObject avatarpanelInput;
    public GameObject tutorialpanelhomepage;
    public bool avatarpanelaktif = false;
    public Button avatarbutton;
    public Button InputPanelavatarbutton;
    public Text identity;
    public Text descriptioncharacter;
    public Slider expBar;
    public InputField createname;
    public GameObject menupanel;
    public GameObject inputpanel;
    public Button newgame;
    public GameObject npc;
    public Button[] buttonavatar;
    private int id;

    public void Awake()
    {

    }
    void Start()
    {

        if (File.Exists(Application.persistentDataPath + SaveLoadData.instance.SaveName))
        {
            menupanel.SetActive(true);
            Debug.Log("ada");
        }
        else
        {
            inputpanel.SetActive(true);
            // npc.SetActive(true);
        }

        //SaveLoadData.instance.Playerdatas.avatar = InputPanelavatarbutton.GetComponent<Image>().sprite;
        CekData();
        opencharacter();
    }

    // Update is called once per frame
    void Update() {

    }

    public void OpenCloseAvatarPanel()
    {
        avatarpanelaktif = !avatarpanelaktif;
        //Set.Active untuk mengubah kondisi object menjadi hidup/mati didalam screen
        avatarpanel.SetActive(avatarpanelaktif);

        if (avatarpanelaktif == false)
        {
            SaveLoadData.instance.Playerdatas.avatar = avatarbutton.GetComponent<Image>().sprite;
            SaveLoadData.instance.SaveData();
        }
    }

    public void OpenCloseAvatarPanelInput()
    {
        avatarpanelaktif = !avatarpanelaktif;
        //Set.Active untuk mengubah kondisi object menjadi hidup/mati didalam screen
        avatarpanelInput.SetActive(avatarpanelaktif);

        if (avatarpanelaktif == false)
        {
            SaveLoadData.instance.Playerdatas.avatar = InputPanelavatarbutton.GetComponent<Image>().sprite;

        }
    }

    public void openscene(string namascene)//function pindah scene easy way
    {
        SceneManager.LoadScene(namascene);
    }

    public void startgame()
    {
        SceneManager.LoadScene("Question 1 Scene");
    }
    public void hiraganalist()
    {
        SceneManager.LoadScene("hiragana A scene");
    }
    public void exitgame()
    {
        Application.Quit();
    }
    public void createaccount()
    {
        SaveLoadData.instance.Playerdatas.namaplayer = createname.text;
        SaveLoadData.instance.SaveData();
        menupanel.SetActive(true);
        SaveLoadData.instance.LoadData();
        CekData();
        inputpanel.SetActive(false);
    }

    public void ChangeCharacterData()
    {
        //SaveLoadData.instance.Playerdatas.namaplayer = createname.text;
        SaveLoadData.instance.SaveData();
        menupanel.SetActive(true);
        SaveLoadData.instance.LoadData();
        CekData();
        inputpanel.SetActive(false);
    }

    void CekData()
    {
        expBar.maxValue = 100;
        identity.text = SaveLoadData.instance.Playerdatas.namaplayer + " / level: " + SaveLoadData.instance.Playerdatas.level.ToString();
        changedescription();
        expBar.value = SaveLoadData.instance.Playerdatas.exp;
        avatarbutton.GetComponent<Image>().sprite = SaveLoadData.instance.Playerdatas.avatar;
        descriptioncharacter.text = gameObject.GetComponent<CharacterDescription>().deskripsikarakter[id];
    }

    void changedescription()
    {
        int level = SaveLoadData.instance.Playerdatas.level;
        id = SaveLoadData.instance.Playerdatas.objectid;
        if(id == 0 && level > 2 && level < 6)
        {
            id = 3;
        }
        else if(id == 1 && level > 2 && level < 6)
        {
            id = 4;
        }
        else if (id == 2 && level > 2 && level < 6)
        {
            id = 5;
        }
        else if (id == 0 && level >= 6)
        {
            id = 6;
        }
        else if (id == 1 && level >= 6)
        {
            id = 7;
        }
        else if (id == 2 && level >= 6)
        {
            id = 8;
        }
    }
    public void opencharacter()
    {
        for(int i = 0; i < buttonavatar.Length; i++)
        {
            buttonavatar[i].interactable = false;
        }

        int h = SaveLoadData.instance.Playerdatas.unlockedcharacter;
        if(h == 0)
        {
            h = 1;
        }
        /*else if(h > 2 && h <= 5)
        {
            h = 2;
        }
        else if(h > 5)
        {
            h = 3;
        }*/
        for(int i = 0; i < h; i++)
        {
            buttonavatar[i].interactable = true;
        }
    }

    public void opentutorialhomepage ()
    {
        if (!File.Exists(Application.persistentDataPath + SaveLoadData.instance.SaveName))
        {
            tutorialpanelhomepage.SetActive(true);
        }
    }
}
