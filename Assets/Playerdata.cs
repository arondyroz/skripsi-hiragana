﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Playerdata {

    public Sprite avatar;
    public int exp;
    public string namaplayer;
    public int level;
    public int objectid;
    public int actID;
    public int unlockedcharacter;
}
