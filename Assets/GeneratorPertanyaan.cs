﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GeneratorPertanyaan : MonoBehaviour {
    public string namalevelselanjutnya;
    public GameObject endgamepanel;
    public Button nextlevel;
    public Slider exp;
    public Text expgaindisplay;
    public Text playerinfo;
    public Image avatar;
    public Image displaypertanyaan;
    public Text displayjawaban1;
    public Text displayjawaban2;
    public Text displayjawaban3;
    public Text displayjawaban4;
    public int idpertanyaan = 0;
    public Text exptext;
    public Pertanyaan[] pertanyaanquiz;
    private int nilaiexpplus = 0;
    private int nilaiexpminus = 0;
    private int expgain = 0;
    // Use this for initialization
    void Start ()
    {
        nextlevel.interactable = false;
        expgaindisplay.text = "Score: 0";
        displaypertanyaan.sprite = pertanyaanquiz[idpertanyaan].pertanyaan;
        displayjawaban1.text = pertanyaanquiz[idpertanyaan].jawaban1;
        displayjawaban2.text = pertanyaanquiz[idpertanyaan].jawaban2;
        displayjawaban3.text = pertanyaanquiz[idpertanyaan].jawaban3;
        displayjawaban4.text = pertanyaanquiz[idpertanyaan].jawaban4;
    }
	
	// Update is called once per frame
	void Update ()
    {
	    
	}
    
    public void jawabpertanyaan(int jawaban)
    {
        
        if(jawaban == pertanyaanquiz[idpertanyaan].idjawaban)
        {
            nilaiexpplus += 20;
            NextPertanyaan();

        }
        else
        {
            nilaiexpminus -= 10;
            NextPertanyaan();
        }

        expgain = nilaiexpplus + nilaiexpminus;
        expgaindisplay.text = "score: " + expgain.ToString();
    }

    void NextPertanyaan()
    {
        if (idpertanyaan < pertanyaanquiz.Length - 1)
        {
            idpertanyaan++;
            displaypertanyaan.sprite = pertanyaanquiz[idpertanyaan].pertanyaan;
            displayjawaban1.text = pertanyaanquiz[idpertanyaan].jawaban1;
            displayjawaban2.text = pertanyaanquiz[idpertanyaan].jawaban2;
            displayjawaban3.text = pertanyaanquiz[idpertanyaan].jawaban3;
            displayjawaban4.text = pertanyaanquiz[idpertanyaan].jawaban4;
        }
        else
        {
            playerinfo.text = SaveLoadData.instance.Playerdatas.namaplayer + " / level: " + SaveLoadData.instance.Playerdatas.level.ToString();
            avatar.sprite = SaveLoadData.instance.Playerdatas.avatar;
            exp.maxValue = 100;
            exp.value = SaveLoadData.instance.Playerdatas.exp;
            endgamepanel.SetActive(true);
            Debug.Log("nilai: " + nilaiexpplus);
            if (nilaiexpplus >= 100)
            {
                
                nextlevel.interactable = true;
                if(namalevelselanjutnya == "Cutscene")
                {
                    SaveLoadData.instance.Playerdatas.actID++;
                }
            }
            StartCoroutine(tambahexp());
        }
    }
    
    IEnumerator tambahexp()
    {
        yield return new WaitForSeconds(0.1f);
        int a = nilaiexpplus + nilaiexpminus;
        exptext.text = "Success\n" + "Score: " + a.ToString();
        //exp.value += (nilaiexpplus + nilaiexpminus);
        //SaveLoadData.instance.Playerdatas.exp = (int)exp.value;
        yield return new WaitForEndOfFrame();
        levelplayer();
        SaveLoadData.instance.SaveData();

    }

    void levelplayer()
    {
        if(exp.value > 99)
        {
            exp.value = 0;
            SaveLoadData.instance.Playerdatas.exp = 0;
            SaveLoadData.instance.Playerdatas.level++;
            playerinfo.text = SaveLoadData.instance.Playerdatas.namaplayer + " / level: " + SaveLoadData.instance.Playerdatas.level.ToString();

        }
    }

    public void pindahlevel ()
    {
        SaveLoadData.instance.SaveData();
        SceneManager.LoadScene(namalevelselanjutnya);
    }
    public void restartgame()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void home()
    {
        SceneManager.LoadScene("Menu Scene");
    }
}
