﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUImanager : MonoBehaviour {

    public int id;
    public Button buttonnext;
    public Text displaynamatabel;
    public Button buttonprevious;
    public GameObject[] tabelhiragana;
    public string[] namatabel;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void openscene(string namascene) //buka macam2 scene
    {
        SceneManager.LoadScene(namascene);
    }
    public void nexthiragana()
    {
        if(id <= tabelhiragana.Length-1)
        {
            id++;
        }
        for(int i = 0;i < tabelhiragana.Length;i++)
        {
            tabelhiragana[i].SetActive(false);
        }
        
        if(id > 0 )
        {
            
            buttonprevious.interactable = true;
        }

        if (id >= tabelhiragana.Length -1)
        {
            buttonnext.interactable = false;
        }
        tabelhiragana[id].SetActive(true);
        displaynamatabel.text = namatabel[id];
    }

    public void previoushiragana()
    {
        if(id > 0)
        {
            id--;
        }
        
        for (int i = 0; i < tabelhiragana.Length; i++)
        {
            tabelhiragana[i].SetActive(false);
        }
        
        if (id < tabelhiragana.Length)
        {
            buttonnext.interactable = true;
            
        }

        if (id < 1)
        {
            Debug.Log("Jalan");
            buttonprevious.interactable = false;
        }
        tabelhiragana[id].SetActive(true);
        displaynamatabel.text = namatabel[id];
    }

}
